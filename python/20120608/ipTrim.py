#coding:utf8

"""
foo.txtからipアドレスを抜き取って表示する（正規表現）
"""
import re 
PATERN = r"A \d+\.\d+\.\d+\.\d+"
r = open("foo.txt","r")


for line in r:
    m = re.findall(PATERN,line)
    if m != None:
        for s in m:
            print s[2:]
        
