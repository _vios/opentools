#coding:utf8
import xml.dom.minidom
import xml
import glob

"""
tcpflow で生成されるreport.xmlから
filesizeが０ではなく，かつdstportが25でない
dstIPを出力する
"""

l = glob.glob("reportxml/*")
iplist = set()
w = open("iplist","w")
count = 0
exceptCount = 0
for f in l:
    try:
        xmldoc = xml.dom.minidom.parse(f)
    except xml.parsers.expat.ExpatError:
        exceptCount += 1
        continue
    flows = xmldoc.getElementsByTagName("fileobject")
    for flow in flows:
        thisip = flow.childNodes[5].getAttribute("dst_ipn")
        if thisip != "192.168.228.240":
            size = flow.childNodes[3].childNodes[0].data
            if size != "0" and flow.childNodes[5].getAttribute("dstport") != "25":
                print flow.childNodes[5].getAttribute("dst_ipn")

