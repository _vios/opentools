#coding:utf8
import sys

"""
連続した同じ行をまとめてカウントする
"""
r = open(sys.argv[1],"r")

domainHash = dict()

for line in r:
    line = line.strip()
    domainHash.setdefault(line,0)
    domainHash[line] += 1


for key in domainHash.keys():
    print key + " : " +str(domainHash[key])

