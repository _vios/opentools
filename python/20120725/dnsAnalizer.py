#coding:utf8
import sqlite3
import commands
import sys
import os

"""
pcapファイルからAレコードに関する
DNS問い合わせを取り出してsqlite DBを作成する
テーブル名:a_query
スキーマは
DNS応答時間（unix time):unitxtime
問い合わせホストのIP：src_ip
DNSサーバのIP：dst_ip
ドメイン名：domain
解決されたIP：resolved_ip

使い方：
第１引数にpcapファイル
第２引数に出力ファイル名


***********************
出力DBの利用例
sqlite3 <出力DB>

で
select * from a_query where domain="<domain name>";
で
<domain name>でクエリを行ったものについて表示する

.quitで終了する
***********************

"""

def makeTable(outputFile):
    con = sqlite3.connect(outputFile)
    sql = """
    create table a_query(
    unixtime integer,
    src_ip text,
    dst_ip text,
    domain text,
    resolved_ip text
    )
    """
    con.execute(sql)
    con.close()

def getArecordFromPcap(pcapFile,outputFile):
    con = sqlite3.connect(outputFile)
    sql = """
    insert into a_query values (?,?,?,?,?)
    """
    r = commands.getoutput("tcpdump -r %s -tt -nn -v src port 53" % (pcapFile))
    r  = r.split("\n")[1:]
    unixTime = 0
    for line in r:
        splited = line.strip().split(" ")
        if unixTime == 0:
            unixTime = splited[0].split(".")[0]
            continue
        else:
            count = 0
            for e in splited:
                if e == "A":
                    #IPが複数返されている場合
                    if splited[count+1][-1] == ",":
                        con.execute(sql,(int(unixTime),splited[0][:splited[0].rfind(".")],splited[2][:splited[2].rfind(".")],splited[count-1][:-1],splited[count+1][:-1]))
                    else:
                        con.execute(sql,(int(unixTime),splited[0][:splited[0].rfind(".")],splited[2][:splited[2].rfind(".")],splited[count-1][:-1],splited[count+1]))
                        break

                count += 1
            unixTime = 0

    con.commit()


if __name__ == "__main__":
    if len(sys.argv) == 3:
        pcapFile = sys.argv[1]
        outputFile = sys.argv[2]
    elif len(sys.argv) == 2:
        pcapFile = sys.argv[1]
        outputFile = pcapFile + ".dnsdb"
    else:
        print "Error:incorrect args"
        sys.exit(-1)

    if os.path.isfile(outputFile):
        print "Error:%s already exists" % outputFile
        getArecordFromPcap(pcapFile,outputFile)
        sys.exit(1)

    makeTable(outputFile)
    getArecordFromPcap(pcapFile,outputFile)
    print "finished"
