#coding:utf8

"""
現在時刻から
定数で指定した時間の
差の「分」を計算する

"""
import time
import datetime

#指定する時間
kYear = 2012
kMonth = 8
kDay = 26
kHour = 7
kMinute = 0
kSecond = 0

def main():
    now = time.time()
    targetTime = time.mktime((kYear,kMonth,kDay,kHour,
        kMinute,kSecond,0,0,0))
    print "%f minutes" % ((targetTime-now)/60)

if __name__ == "__main__":
    main()
